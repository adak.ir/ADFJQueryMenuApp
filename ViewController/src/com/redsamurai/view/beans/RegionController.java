package com.redsamurai.view.beans;

import java.io.Serializable;

import oracle.adf.controller.TaskFlowId;
import oracle.adf.view.rich.render.ClientEvent;

public class RegionController implements Serializable {
    private String taskFlowId = "/WEB-INF/dashboard-flow.xml#dashboard-flow";

    public RegionController() {
        super();
    }

    public TaskFlowId getDynamicTaskFlowId() {
        return TaskFlowId.parse(taskFlowId);
    }

    public void setDynamicTaskFlowId(String taskFlowId) {
        this.taskFlowId = taskFlowId;
    }
    
    public void menuSelectionListener(ClientEvent clientEvent) {
        String menuKey = (String) clientEvent.getParameters().get("menuKey"); 
        setDynamicTaskFlowId(menuKey);
    }
}
